var angularPage=require('../pages/itemsGridPage.js');
var chai = require('chai'),
    expect = chai.expect,
    chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

//var {defineSupportCode} = require('cucumber');
let scenarioTimeout = 200 * 1000,
    {defineSupportCode} = require('cucumber');

defineSupportCode(({setDefaultTimeout}) => {
    setDefaultTimeout(scenarioTimeout);
});