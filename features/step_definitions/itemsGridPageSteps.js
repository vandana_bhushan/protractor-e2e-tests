var angular =require('../pages/itemsGridPage.js');
var chai = require('chai'),
    expect = chai.expect,
    chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);


//var {defineSupportCode} = require('cucumber');
let scenarioTimeout = 200 * 1000,
    {defineSupportCode} = require('cucumber');

defineSupportCode(({setDefaultTimeout}) => {
    setDefaultTimeout(scenarioTimeout);
});
defineSupportCode(({Given, When, Then}) => {
  Given(/^she is on the items grid view page$/, function() {
	  //browser.pause(5005);
	return browser.getCurrentUrl().then(function(text){expect(text).contain("/Items")});
	  
  }),
    Given(/^she filters the "(.*?)" items$/, function(D03) {
	  angular.clickAddFilter();
	  angular.enterField(D03);
	  browser.pause();
	  	  
  })
  
      Given(/^the where used is clicked$/, function() {
	  angular.clickhamburger();
	  angular.clickwhereused();
	  	  
  })
})