var itemsGridPage = require('../pages/itemsGridPage.js');
var homepage = require('../pages/homePage.js');
var chai = require('chai');

var expect = chai.expect; // Using Expect style 


protractorImageComparison = require('protractor-image-comparison');

const itemsTabscreen = element(by.id('nav-Inventory-Items'));
const hamburger = element(by.xpath('//div[@class="mainContainer layout-column"]/md-content/ui-view/items/items-tool-bar/div/md-menu[2]/a'));
const whereused = element(by.css("md-menu-content>md-menu-item"));
const whereusedpopup = element(by.css("md-dialog"));
const distinctvaluespopup = element(by.css("md-dialog"));
const showcolumnspopup = element(by.css("md-dialog"));
const addfilterpopup = element(by.css("md-dialog"));
const taggeditems = element(by.xpath('//div[2]/md-content/ui-view/items/items-tool-bar/div/md-menu[1]/a'));
const recalcicon = element(by.xpath('//div[2]/md-content/ui-view/items/items-tool-bar/div/recalc-button'));
const endsessionicon = element(by.xpath('//div[2]/md-content/ui-view/items/items-tool-bar/div/end-session-button/div'));
const endsessionpopup = element(by.css("md-dialog"));
const endsessionclose = element(by.css("md-toolbar")).element(by.xpath('//div[@class="md-toolbar-tools"]/a'));
const whereusedclose = element(by.css("md-dialog")).element(by.xpath('//div[@class="md-toolbar-tools"]/a'));
const gridediticon = element(by.xpath('//div[2]/md-content/ui-view/items/items-tool-bar/div/div[3]/a'));
const approvebtn = element(by.xpath('//div[2]/md-content/ui-view/items/items-tool-bar/div/approve-button/div/div'));
const deferbtn = element(by.xpath('//div[2]/md-content/ui-view/items/items-tool-bar/div/defer-button/div'));
const graphsection = element(by.xpath('//div[@class="itemsMetrics"]/div/div'));
const addfilteroptionsmenu = element(by.css("md-menu-content"));


let scenarioTimeout = 200 * 1000,
    {
        defineSupportCode
    } = require('cucumber');
defineSupportCode(({
    setDefaultTimeout
}) => {
    setDefaultTimeout(scenarioTimeout);
});
defineSupportCode(({
    Given, When, Then
}) => {
    Given(/^items tab matches the image$/, function() {

        return expect(browser.protractorImageComparison.checkElement(itemsTabscreen, 'itemsTab')).to.eventually.equal(0);

    });

    Given(/^hamburger icon matches the image$/, function() {
        if (expect(browser.protractorImageComparison.checkElement(hamburger, 'hb')).to.eventually.equal(0)) {
            console.log("Image match for Hamburger");
        }
        return true;
    });

    Given(/^she navigates to the items tab$/, function() {
        console.log("clicking items tab");
        return homepage.clickItemsTab();
    });

    Given(/^whereused icon matches the image$/, function() {

        browser.waitForAngular();

        return expect(browser.protractorImageComparison.checkElement(whereusedpopup, 'whereusedpopup')).to.eventually.equal(0);

    });


    Given(/^she clicks on the hamburger icon$/, function() {

        return itemsGridPage.clickhamburger();

    });

    Given(/^she clicks on whereused$/, function() {
        browser.waitForAngular();
        return itemsGridPage.clickWhereused();

    });


    Then(/^the Tagged items icon matches the image$/, function() {
        return expect(browser.protractorImageComparison.checkElement(taggeditems, 'tagicon')).to.eventually.equal(0);
    });


    Then(/^the recalculate icon matches the image$/, function() {
        return expect(browser.protractorImageComparison.checkElement(recalcicon, 'recalcicon')).to.eventually.equal(0);

    });


    Then(/^end session icon matches the image$/, function() {
        browser.waitForAngular();
        return expect(browser.protractorImageComparison.checkElement(endsessionicon, 'endsessionicon')).to.eventually.equal(0);
    });

    Given(/^the end session is clicked$/, function() {

        return endsessionicon.click();

    });


    Then(/^the end session pop up matches the image$/, function() {
        browser.waitForAngular();

        return expect(browser.protractorImageComparison.checkElement(endsessionpopup, 'sessionpopup')).to.eventually.equal(0);
    });

    Then(/^she clicks on the close icon on the end session$/, function() {
        return itemsGridPage.clicksessionclose();
    });

    Then(/^the grid edit icon matches the image$/, function() {
        return expect(browser.protractorImageComparison.checkElement(gridediticon, 'gridedit')).to.eventually.equal(0);
    });

    Then(/^the approve button matches the image$/, function() {
        return expect(browser.protractorImageComparison.checkElement(approvebtn, 'approvebtn')).to.eventually.equal(0);
    });


    Then(/^the defer button matches the image$/, function() {
        return expect(browser.protractorImageComparison.checkElement(deferbtn, 'deferbtn')).to.eventually.equal(0);
        
    });

    When(/^she clicks on cancel on where used pop up$/, function() {
        return whereusedclose.click();
    });

    When(/^she clicks on the distinct values pop up$/, function() {
        return itemsGridPage.clickdistinctvalues();
    });

    When(/^the distinct values pop up matches the image$/, function() {
        browser.waitForAngular();

        return expect(browser.protractorImageComparison.checkElement(distinctvaluespopup, 'distinctvaluespopup')).to.eventually.equal(0);
    });

    Then(/^she clicks on the cancel on the distinct values pop up$/, function() {
        return itemsGridPage.clickcanceldistinctvalues();
    });

    Then(/^Trixy clicks on settings icon$/, function() {

        return itemsGridPage.clicksettingsicon();
    });

    Then(/^clicks on show columns$/, function() {
        return itemsGridPage.clickshowcolumns();
    });


    Then(/^the show columns pop up matches the image$/, function() {
        browser.waitForAngular();
        console.log("waiting for angular");
        return expect(browser.protractorImageComparison.checkElement(showcolumnspopup, 'showcolumnspopup')).to.eventually.equal(0);
    });

    Then(/^she clicks on cancel on the pop up$/, function() {
        return itemsGridPage.clickcancelshowcolumns();
    });

    Then(/^she selects the toggle for graphs$/, function() {
        return itemsGridPage.clicktogglegraph();
    });


    When(/^the graph section matches the image$/, function() {
        browser.waitForAngular();
        console.log("waiting for angular");
        return expect(browser.protractorImageComparison.checkElement(graphsection, 'graphsection')).to.eventually.equal(0);
    });

    Then(/^Trixy clicks on the add filter$/, function() {
        return itemsGridPage.clickAddFilter();
    });


    When(/^add filter pop up matches the image$/, function() {
        return expect(browser.protractorImageComparison.checkElement(addfilterpopup, 'addfilterpopup')).to.eventually.equal(0);
    });

    When(/^she clicks on the cancel on the filter pop up$/, function() {
        return itemsGridPage.clickcancelfilterpopup();
    });

    When(/^Trixy clicks on the options for filters$/, function() {
        return itemsGridPage.addfilteroptions();
    });


    When(/^the options match the image$/, function() {
        return expect(browser.protractorImageComparison.checkElement(addfilteroptionsmenu, 'addfilteroptionsmenu')).to.eventually.equal(0);
    });


    When(/^end session is clicked$/, function() {
        return itemsGridPage.clickendsession();
    });

    When(/^she clicks on the close icon on end session$/, function() {
        return endsessionclose.click();
    });

})