var angular =require('../pages/homePage.js');
var chai = require('chai'),
    expect = chai.expect,
    chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);


//var {defineSupportCode} = require('cucumber');
let scenarioTimeout = 200 * 1000,
    {defineSupportCode} = require('cucumber');

defineSupportCode(({setDefaultTimeout}) => {
    setDefaultTimeout(scenarioTimeout);
});
defineSupportCode(({Given, When, Then}) => {
  Given(/^Trixy is on inventory home page$/, function() {
	  //browser.pause(5003);
	return browser.getCurrentUrl().then(function(text){expect(text).contain("/Home")});
	  
  })
  

})