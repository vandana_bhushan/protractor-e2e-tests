var itemsGridPage =require('../pages/itemsGridPage.js');
var homepage =require('../pages/homePage.js');
var chai = require('chai');
var assert = chai.assert;    // Using Assert style 
var expect = chai.expect;    // Using Expect style 
var should = chai.should();  // Using Should style 

protractorImageComparison = require('protractor-image-comparison');
const items='items'
const itemsTab=element(by.id('nav-Inventory-Items'));

	   browser. protractorImageComparison = new protractorImageComparison(
            {
             autoSaveBaseline: true,
			 baselineFolder: 'path/to/baseline/',
             screenshotPath: 'path/to/save/actual/screenshots/'
            }
        );
	  
let scenarioTimeout = 200 * 1000,
    {defineSupportCode} = require('cucumber');
	defineSupportCode(({setDefaultTimeout}) => {
    setDefaultTimeout(scenarioTimeout);
});
