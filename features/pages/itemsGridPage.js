'use strict';
module.exports = {

    itemsGridPage: {


        AddFilter: element(by.xpath('//div[@class="itemsToolbar"]/div[2]/div/button')),
        enterField: element(by.id('input-139')),
        hamburger: element(by.xpath('//div[@class="mainContainer layout-column"]/md-content/ui-view/items/items-tool-bar/div/md-menu[2]/a')),
        taggeditems: element(by.xpath('//div[2]/md-content/ui-view/items/items-tool-bar/div/md-menu[1]/a')),
        recalcicon: element(by.xpath('//div[@class="itemsToolbar"]/div/recalc-button/div/a')),
        endsessionpopup: element(by.xpath('//div[@id="tab-content-33"]/div/md-content/div')),
        approvebtn: element(by.xpath('//div[@class="itemsToolbar"]/div/approve-button/div')),
        deferbtn: element(by.xpath('//div[@class="itemsToolbar"]/div/defer-button/div/div/div')),
        showclmns: element(by.xpath('//div[@id="dialogContent_42"]/edit-columns-dialog/form/md-dialog-content/dual-list')),
        showhidecolumns: element(by.xpath('//div[@id="menu_container_9"]/md-menu-content/md-menu-item[1]/button')),
        graphsection: element(by.xpath('//div[2]/md-content/ui-view/items/key-metrics/div/div/div')),
        addfilterpopup: element(by.xpath('//*[@id="dialogContent_43"]/add-filter-dialog/form/md-dialog-content')),
        filteroption: element(by.xpath('//*[@id="menu_container_9"]/md-menu-content')),
        endsessionicon: element(by.xpath('//div[@class="itemsToolbar"]/div/end-session-button/div/a')),
        endsessionclose: element(by.xpath('//*[@id="dialogContent_37"]/end-session-dialog/form/md-toolbar/div/a')),
        whereused: element(by.css("md-menu-content")).element(by.xpath('//button[.="Where Used"]')),
        distinctvalues: element(by.css("md-menu-content")).element(by.xpath('//button[.="Distinct Values"]')),
        whereusedclose: element(by.css("md-toolbar ")).element(by.xpath('//div[@class="dialogContent_68"]/where-used-dialog/form/md-toolbar/div/a')),
        distinctvaluescancel: element(by.xpath('//distinct-values-dialog/form/md-toolbar/div/a')),
        cancelfilterpopup: element(by.xpath('//add-filter-dialog/form/md-toolbar/div/a')),
        cancelshowcolumns: element(by.xpath('//edit-columns-dialog/form/md-toolbar/div/a')),
        //distinctvaluescancel: element(by.xpath('//*[@id="menu_container_11"]/md-menu-content/md-menu-item[1]/button')),
        togglegraph: element(by.css("md-menu-content")).element(by.xpath('//button[.="Toggle Graphs On/Off"]')),
        settingsicon: element(by.xpath('//div[2]/md-content/ui-view/items/items-tool-bar/div/md-menu-bar/md-menu')),
        addfilter: element(by.xpath('//div[@class="items-tool-bar"]/div/div[2]/div/button')),
        addfilteroptions: element(by.css("md-menu>button")),
    },



    clickAddFilter: function(addfilter) {

        this.itemsGridPage.addfilter.click();
    },

    clickhamburger: function() {
        console.log("In click hb");
        return this.itemsGridPage.hamburger.click();

    },

    clickWhereused: function() {
        console.log("click where used ");
        return this.itemsGridPage.whereused.click();
    },


    enterField: function(text) {
        page.enterField.click().sendKeys(text);
        //page.enterField.sendKeys("Status Type");
    },


    /*   clickAddFilter: function(AddFilter){
         return this.itemsGridPage.AddFilter.click(AddFilter);
      }, */


    clickendsession: function(endsessionicon) {
        return this.itemsGridPage.endsessionicon.click();
    },


    clicksessionclose: function(endsessionclose) {
        return this.itemsGridPage.endsessionclose.click();
    },


    clickcancelwhereused: function(whereusedclose) {
        console.log("clicking where used cancel");
        return this.itemsGridPage.whereusedclose.click();
    },


    clickdistinctvalues: function(distinctvalues) {
        console.log("clicking distinct values");
        return this.itemsGridPage.distinctvalues.click();
    },


    clickcanceldistinctvalues: function() {

        return this.itemsGridPage.distinctvaluescancel.click();
    },


    clicksettingsicon: function(settingsicon) {
        return this.itemsGridPage.settingsicon.click();
    },

    clickshowcolumns: function(showhidecolumns) {
        return this.itemsGridPage.showhidecolumns.click();
    },


    clickcancelshowcolumns: function(cancelshowcolumns) {
        return this.itemsGridPage.cancelshowcolumns.click();
    },

    clickcancelfilterpopup: function(cancelfilterpopup) {
        return this.itemsGridPage.cancelfilterpopup.click();
    },

    clicktogglegraph: function(togglegraph) {
        return this.itemsGridPage.togglegraph.click();
    },

    addfilteroptions: function(addfilteroptions) {
        return this.itemsGridPage.addfilteroptions.click();
    }


};