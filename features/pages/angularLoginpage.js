'use strict';
module.exports = {

  angularLoginpage: {
    username: element(by.id('username')),
    signinButton: element(by.id('btnSignIn')),
    password: element(by.id('password'))
  },

  go: function(site) {
    browser.get(site);
  },

  enterUsername: function(username) {
    var angular = this.angularLoginpage;
    angular.username.sendKeys(username);
  },
  
    enterPassword: function(password) {
    var angular = this.angularLoginpage;
    angular.password.sendKeys(password);
  },

  signin: function() {
    var angular = this.angularLoginpage;
    angular.signinButton.click();
  }
};