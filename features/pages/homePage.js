'use strict';
var chai = require('chai');
var assert = chai.assert;    // Using Assert style 
var expect = chai.expect;    // Using Expect style 
var should = chai.should();  // Using Should style 
module.exports = {

  InventoryHomepage: {
    title: element(by.css('h3')),
	itemsTab: element(by.id('nav-Inventory-Items')),
	
    
  },

  checkTitle: function(title) {
    return expect(page.getContents()).to.contain(title); 
  },

  clickItemsTab: function(itemsTab){
	 var page = this.InventoryHomepage;
	 console.log("homepage click");
	 return page.itemsTab.click();
 
  }
};