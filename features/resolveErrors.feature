#features/test.feature
Feature: To test the items feature of the inventory application
	As an inventory user
	I want to be able to resolve errors on items
	So taht I can export items.

	Scenario: Resolve "D03" error
		Given I go to "https://ekati.staging.myoniqua.com"
		Given Trixy is on inventory home page
		And she navigates to the items tab
		Then she is on the items grid view page
		When she filters the "D03" items
