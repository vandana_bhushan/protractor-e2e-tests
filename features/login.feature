 Feature: Login
	As a valid user
	I should be able to login
	In order to perform actions
 
 Scenario: Test valid login
 Given I go to "https://ekati.staging.myoniqua.com"
 When I enter "ekttest" as username
 When I enter "Admin1" as password
 Then I click the signin button