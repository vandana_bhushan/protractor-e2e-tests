Feature: cross browser test

Scenario: cross browser test
	
		Given I go to "https:ekati.staging.myoniqua.com"
		Given Trixy is on inventory home page
		And she navigates to the items tab
		Then items tab matches the image
		
		And the Tagged items icon matches the image
		And the recalculate icon matches the image
		
		And end session icon matches the image
		When the end session is clicked 
		Then the end session pop up matches the image
		And she clicks on the close icon on end session
		
		And the grid edit icon matches the image
		
		And the grid edit icon matches the image
		And the approve button matches the image
		And the defer button matches the image
				
		Then hamburger icon matches the image
		When she clicks on the hamburger icon
		When she clicks on whereused
		Then whereused icon matches the image
		Then she clicks on cancel on where used pop up
		
		When she clicks on the hamburger icon
		And she clicks on the distinct values pop up
		Then the distinct values pop up matches the image
		Then she clicks on the cancel on the distinct values pop up
		
		When Trixy clicks on settings icon
		And clicks on show columns
		Then the show columns pop up matches the image
		And she clicks on cancel on the pop up
		
		When Trixy clicks on settings icon
		And she selects the toggle for graphs
		Then the graph section matches the image
		And Trixy clicks on settings icon
		Then she selects the toggle for graphs
		
		When Trixy clicks on the add filter
		Then add filter pop up matches the image
		Then she clicks on the cancel on the filter pop up
		
		When Trixy clicks on the options for filters
		Then the options match the image
		