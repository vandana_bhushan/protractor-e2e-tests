//protractor.conf.js
exports.config = {

  seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
  //seleniumArgs: ['-Dwebdriver.ie.driver= C:\Users\vandanabh\Downloads\IEDriverServer_Win32_3.5.1\IEDriverServer.exe'],
  getPageTimeout: 60000,
  allScriptsTimeout: 500000,
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
   
 multiCapabilities: [{
  'browserName': 'chrome'
   
}, {
  'browserName': 'firefox'
  
}],
maxSessions:1,

  // Spec patterns are relative to this directory.
  specs: [
    'features/browserTest.feature',
],
   allScriptsTimeout: 50000,
   getPageTimeout: 50000, //This is the Page timeout

  baseURL: 'http://localhost:8080/',
  
      onPrepare: function() {
		console.log("On prepare");
		browser.driver.manage().window().setSize(1440, 900);
        const protractorImageComparison = require('protractor-image-comparison');
        browser. protractorImageComparison = new protractorImageComparison(
            {
			 baselineFolder: 'path/to/baseline/',
             screenshotPath: '.path/to/save/actual/screenshots/',
			 formatImageName: '{tag}_{width}-{height}',
			 //autoSaveBaseline: true,
			 debug: true
            }
        );
    },
	
    cucumberOpts: {
    require: 'features/step_definitions/*.js',
    tags: false,
    format: 'pretty',
    profile: false,
    'no-source': true
  }
};